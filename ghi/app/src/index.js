import React from 'react';
import ReactDOM from 'react-dom/client';
import './index.css';
// The next line imports the App function from the App.js file that you just changed
import App from './App';
import reportWebVitals from './reportWebVitals';
// Then, that stuff about 'root' and 'root.render' is telling React where to take that fake HTML and put it.
const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>
);

//add to your JavaScript so that it checks if the response is okay. If it's okay, then your code gets the data from the responses json method. Then, it prints the data to the console. If the response is not okay, it prints the response as an error.//
async function loadAttendees() {
  const response = await fetch('http://localhost:8001/api/attendees/');
  if (response.ok) {
    const data = await response.json();
    root.render(
      <React.StrictMode>
        <App attendees={data.attendees} />
        {/* sets a variable named 'attendees' to the value inside the curly braces. Then, that variable becomes a property on the props parameter that gets passed into the function */}
      </React.StrictMode>
    );
  } else {
    console.error(response);
  }
}
loadAttendees();



// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
